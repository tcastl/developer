# Motili Developer Application Submission

> A Vue.js project

![app image](app.png)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Lighthouse Scores
![Progressive Web App - 91, Performance - 80, Accessibility - 91, Best Practices - 92](lighthouse.png)

## Deployed Site
[here](https://expansionpanelsjobdescription.firebaseapp.com)