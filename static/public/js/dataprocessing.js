import * as index from './index.js'
import changeCase from 'change-case'
var data = []
var labels = []
for (var i in index.job.profile) {
  data.push(index.job.profile[i])
}
for (var x = 0; x < Object.keys(index.job.profile).length; x++) {
  labels.push(changeCase.titleCase(Object.keys(index.job.profile)[x]))
}
export var chartArray = data
export var chartLabels = labels
