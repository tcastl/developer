//step 1: export everything
//step 2: manually lowerCamelCase necessary word breaks in job keys

export var job = {
	"headline": "Full Stack Software Developer",
	"essentials": {
		"locations": "denver",
        "employment": EmploymentType().Permanent,
        "experience": [ExperienceLevels().Junior, ExperienceLevels().Seasoned],
		"startDate": (new Date()).getTime(),
		"companySize": CompanySize().TwentyToFifty,
		"teamSize": { "min": 1, "max": 6 },
	},
	"methodology": {
		"codeReviews": true,
		"prototyping": true,
		"pairProgramming": true,
		"failFast": true,
		"unitTests": true,
		"integrationTests": true,
		"buildServer": BuildServers().Codeship,
		"staticCodeAnalysis": CodeAnalysisTools().NotYetChosen,
		"versionControl": VersionControlSystem().BitBucket,
		"issueTracker": IssueTrackers().Jira,
		"knowledgeRepo": KnowledgeRepos().Confluence,
		"standups": true,
		"QAProtocol": true,
		"freedomOverTools": true,
		"oneCommandBuild": true,
		"quickstart": true,
		"commitOnDayOne": true,
	},
	"specs": {
		"workload": 1.0,
		"workWeek": 40,
        "coreHours": { from: 800, to: 1700 },
        "schedule": ScheduleOptions().Flexible,
        "remote": RemoteWorking().Negotiable,
        "PTO": PTO().Unlimited
	},
	"profile": {
		"newFeatures": 50,
		"clientSupport": 9,
		"documentation": 10,
		"maintenance": 30,
		"meetings": 1,
	},
	"equipment": {
		"operatingSystem": [OperationSystems().MacOSX, OperationSystems().CentOS],
		"computer": MachineType().Laptop,
		"monitors": Monitors().Negotiable,
	},
	"technologies": {
        "junior": {
		    "CSS3": Level().Good,
		    "HTML5": Level().Good,
		    "javascript": Level().Good,
		    "node": Level().Good,
		    "REST": Level().Good,
		    "UI/UX": Level().Familiar,
            "design": Level().Familiar,
            "testing": {
                "oneOf": {
                    "junit": Level().Good,
                    "mocha": Level().Good,
                    "jasmine": Level().Good,
                    "selenium": Level().Good,
                }
            },
            "framework": {
                "oneOf": {
                    "react": Level().Familiar,
                    "vue": Level().Familiar,
                    "angular": Level().Familiar,
                }
            },
		    "boardGames": Level().Familiar,
        },
        "seasoned": {
		    "CSS3": Level().Expert,
		    "HTML5": Level().Expert,
		    "javascript": Level().Expert,
		    "node": Level().Expert,
		    "REST": Level().Expert,
		    "UI/UX": Level().Good,
            "design": Level().Good,
            "testing": {
                "oneOf": {
                    "junit": Level().Good,
                    "mocha": Level().Good,
                    "jasmine": Level().Good,
                    "selenium": Level().Good,
                }
            },
            "framework": {
                "oneOf": {
                    "react": Level().Familiar,
                    "vue": Level().Familiar,
                    "angular": Level().Familiar,
                }
            },
		    "boardGames": Level().Familiar,
        }

    },
    "bonusPoints": {
        "devOps": Level().Good,
        "SQL": Level().Good,
        "mobileDevelopment": Level().Good,
        "quotingBadActionMovies": Level().Expert
    },
	"other": [
        "we love technology",
        "we solve interesting problems"
	],
	"misc": {
		"training": TrainingType().Informal,
		"teamEvents": true,
        "ecopass": true,
        "healthcare": true,
        "dental": true,
		"mobilePhone": false,
        "kitchen": true,
		"freeStuff": ["coffee (lots)", "beverages (adult and otherwise)", "snacks", "bike parking"],
	}
}

export function EmploymentType() { return enumerate("Permanent", "Temporary", "Project"); }
export function ExperienceLevels() { return enumerate("Junior", "Seasoned", "Lead", "GrayBeard"); }
export function CompanySize() { return enumerate("LessThanTen", "TenToTwenty", "TwentyToFifty", "FiftyToTwoHundred", "MoreThanTwoHundred"); }
export function VersionControlSystem() { return enumerate("NotYetChosen", "Git", "BitBucket"); }
export function IssueTrackers() { return enumerate("NotYetChosen", "GitHub", "Jira"); }
export function BuildServers() { return enumerate("NotYetChosen", "Jenkins", "Travis", "Codeship"); }
export function CodeAnalysisTools() { return enumerate("NotYetChosen", "ESLint"); }
export function KnowledgeRepos() { return enumerate("NotYetChosen", "GitHubWiki", "Confluence"); }
export function TravelOptions() { return enumerate("None", "Possible", "Plentiful"); }
export function ScheduleOptions() { return enumerate("Fixed", "Flexible"); }
export function RemoteWorking() { return enumerate("No", "Negotiable", "Required"); }
export function RelocationPackages() { return enumerate("Nonealse", "Negotiable"); }
export function OperationSystems() { return enumerate("MacOSX", "CentOS", "Ubuntu", "Windows"); }
export function MachineType() { return enumerate("Workstation", "Laptop"); }
export function Monitors() { return enumerate("Negotiable"); }
export function Level() { return enumerate("Familiar", "Good", "Expert"); }
export function TrainingType() { return enumerate("None", "Informal", "Formal", "External"); }
export function PTO() { return enumerate("Accrued", "Unlimited") }
//step 3: add var keyword to get enumerate function to work with webpack
// https://github.com/RougeWare/Micro-JS-Enum/tree/master/lib
export function enumerate() { var v=arguments; var s={all:[],keys:v};for(var i=v.length;i--;)s[v[i]]=s.all[i]=v[i];return s };
